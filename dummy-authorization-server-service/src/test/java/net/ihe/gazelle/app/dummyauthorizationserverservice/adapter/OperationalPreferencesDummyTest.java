package net.ihe.gazelle.app.dummyauthorizationserverservice.adapter;

import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class OperationalPreferencesDummyTest {

    /**
     * Test wanted mandatroy preferences list.
     */
    @Test
    void wantedMandatoryPreferences() {
        OperationalPreferencesClientApplication operationalPreferencesService = new OperationalPreferencesDummy();

        Map wantedMandatoryPreferences = operationalPreferencesService.wantedMandatoryPreferences();

        assertNotNull(wantedMandatoryPreferences, "Wanted Mandatory Preferences map shall not be null !");
        assertEquals(0, wantedMandatoryPreferences.entrySet().size(), "Wanted Mandatory Preferences map shall be empty");
    }
}