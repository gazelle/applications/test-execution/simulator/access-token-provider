package net.ihe.gazelle.app.dummyauthorizationserverservice.adapter;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Tests for {@link AuthorizationServerService}
 */
class AuthorizationServerServiceTest {

    private static final String SUBJECT = "aamrein";
    private static final String AUDIENCE = "audience";

    @Test
    public void defaultConstructorTest() {
        AuthorizationServerService authorizationServerService = new AuthorizationServerService();
        assertNotNull(authorizationServerService);
    }

    @Test
    public void initConstructorTest() {
        AuthorizationServerService authorizationServerService = new AuthorizationServerService((String audience) -> "myBeautifulKeyWhichIsAJWTSecretSoSecret");
        assertNotNull(authorizationServerService);
    }

    /**
     * Test the generation of a token
     */
    @Test
    public void getAccessToken() {
        AuthorizationServerService authorizationServerService = new AuthorizationServerService((String audience) -> "myBeautifulKeyWhichIsAJWTSecretSoSecret");
        assertNotNull(authorizationServerService.getAccessToken(SUBJECT, AUDIENCE, null, null),
                "Provided access token shall not be null !");
    }
}