package net.ihe.gazelle.app.dummyauthorizationserverservice.adapter;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class DummyAuthorizationServerServiceApplicationTest {

    /**
     * Test class getter.
     */
    @Test
    void getClasses() {
        DummyAuthorizationServerServiceApplication application = new DummyAuthorizationServerServiceApplication();

        Set classes = application.getClasses();

        assertNotNull(classes, "Classes map shall not be null !");
        assertEquals(1, classes.size(), "Classes map shall contain a single element !");
    }
}