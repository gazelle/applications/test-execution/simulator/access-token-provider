package net.ihe.gazelle.app.dummyauthorizationserverservice.adapter;

import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Define mandatory preferences.
 */
public class OperationalPreferencesDummy implements OperationalPreferencesClientApplication {

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {
        return new HashMap<>();
    }
}
