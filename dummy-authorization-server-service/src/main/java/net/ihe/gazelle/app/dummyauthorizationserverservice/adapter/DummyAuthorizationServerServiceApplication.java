package net.ihe.gazelle.app.dummyauthorizationserverservice.adapter;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Our Application
 */
@ApplicationPath("/")
public class DummyAuthorizationServerServiceApplication extends Application {

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<>();
        s.add(AuthorizationServerService.class);
        return s;
    }
}
