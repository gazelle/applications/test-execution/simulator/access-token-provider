package net.ihe.gazelle.app.dummyauthorizationserverservice.adapter;


import net.ihe.gazelle.app.accesstokenproviderapi.application.DummyAuthzServer;
import net.ihe.gazelle.app.accesstokenproviderapi.application.DummyAuthzServerSoapui;
import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.ext.Provider;

/**
 * Service for Mock Access Token Provider.
 */
@Provider
@Path(value = "/mock-token")
public class AuthorizationServerService {

    private DummyAuthzServer dummyAuthzServer;

    /**
     * Default constructor
     *
     */
    public AuthorizationServerService() {
        //EMPTY
    }

    /**
     * Injected constructor.
     *
     * @param audienceSecretRetriever audience secret retriever
     */
    @Inject
    public AuthorizationServerService(AudienceSecretRetriever audienceSecretRetriever) {
        dummyAuthzServer = new DummyAuthzServerSoapui();
        ((DummyAuthzServerSoapui) dummyAuthzServer).setAudienceSecretRetriever(audienceSecretRetriever);
    }

    /**
     * Setter for the dummyAuthzServer property.
     *
     * @param dummyAuthzServer value to set to the property.
     */
    public void setDummyAuthzServer(DummyAuthzServer dummyAuthzServer) {
        this.dummyAuthzServer = dummyAuthzServer;
    }

    /**
     * get a dummy access token
     *
     * @param userId user id
     * @param audienceId audience id
     * @param purposeOfUse purpose of use
     * @param resourceId resource id
     * @return an access token
     */
    @GET
    public byte[] getAccessToken(@QueryParam("userId") String userId, @QueryParam("audienceId") String audienceId,
                                 @QueryParam("purposeOfUse") String purposeOfUse, @QueryParam("resourceId") String resourceId) {
        return dummyAuthzServer.getAccessToken(userId, audienceId, purposeOfUse, resourceId);
    }

}
