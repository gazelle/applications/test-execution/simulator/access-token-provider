# Dummy Authorization Server Service

This service mock an Access Token Provider, giving a mocked token using one of available user id 
and a secret that can be configured depending on the used audience.

## Request examples 

```http://localhost:8780/authorization-server/mock-token?userId=aamrein&audienceId=audience```

| Parameter Name  | Usage                                                             |
|-------------|-----------------------------------------------------------------------|
| userId      | User for whom the token is generated                                  |
| audienceId  | ID of the audience used to retrieve secret in Gazelle configurations. |

The response body to this request will be the content of the generated token.

## Available user ID

| User ID  |
|----------|
| aamrein  |
| aerne    |

## Install the tool

Sources are available [here](https://gitlab.inria.fr/gazelle/applications/test-execution/simulator/access-token-provider)

Deploy the `app.dummy-authorization-server-service-X.X.X.war` artifact in a wildfly 18 server. 
No specific wildfly configuration is needed for the tool to work.

## Configure Audiences

Edit the file `/opt/simulators/audience.properties`, add a property for each audience that needs to be configured :

```
audience1=secret1
audience2=secret2
audience3=secret3
...
```