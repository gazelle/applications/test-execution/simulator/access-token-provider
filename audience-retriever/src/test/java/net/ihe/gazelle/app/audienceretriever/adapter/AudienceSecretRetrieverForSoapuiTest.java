package net.ihe.gazelle.app.audienceretriever.adapter;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Class to test the retrieve of the audience secret
 */
class AudienceSecretRetrieverForSoapuiTest {

    @Test
    public void defaultConstructorTest() {
        AudienceSecretRetrieverForSoapui audienceSecretRetrieverForSoapui = new AudienceSecretRetrieverForSoapui();
        assertNotNull(audienceSecretRetrieverForSoapui);
    }

    /**
     * Test the property set in the retriever secret audience method
     */
    @Test
    void retrieveSecretForAudienceWithPropertiesPathTest() {
        AudienceSecretRetrieverForSoapui audienceSecretRetrieverForSoapui = new AudienceSecretRetrieverForSoapui("/opt/simulators/audience" +
                ".properties");
        assertNull(audienceSecretRetrieverForSoapui.retrieveSecretForAudience("monpetitsecret"));
    }


}
