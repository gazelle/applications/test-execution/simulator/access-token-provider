package net.ihe.gazelle.app.audienceretriever.adapter;

import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AudienceSecretRetrieverImplTest {

    AudienceSecretRetrieverImpl impl = new AudienceSecretRetrieverImpl();

    @Test
    public void defaultConstructorTest() {
        AudienceSecretRetrieverImpl impl = new AudienceSecretRetrieverImpl();
        assertNotNull(impl);
    }

    @Test
    public void setPreferencesAndRetrieveAudienceTest() {
        String myValue = "myValue";
        OperationalPreferencesService service = new OperationalPreferencesService() {
            @Override
            public boolean getBooleanValue(String s, String s1) throws PreferenceException, NamespaceException {
                return false;
            }

            @Override
            public int getIntegerValue(String s, String s1) throws PreferenceException, NamespaceException {
                return 0;
            }

            @Override
            public String getStringValue(String s, String s1) throws PreferenceException, NamespaceException {
                return myValue;
            }
        };
        impl.setPreferencesService(service);
        assertEquals(myValue, impl.retrieveSecretForAudience(null));

    }

    @Test
    public void setPreferencesAndRetrieveAudiencePreferenceExceptionTest() {
        OperationalPreferencesService service = new OperationalPreferencesService() {
            @Override
            public boolean getBooleanValue(String s, String s1) throws PreferenceException, NamespaceException {
                return false;
            }

            @Override
            public int getIntegerValue(String s, String s1) throws PreferenceException, NamespaceException {
                return 0;
            }

            @Override
            public String getStringValue(String s, String s1) throws PreferenceException, NamespaceException {
                throw new PreferenceException();
            }
        };
        impl.setPreferencesService(service);
        assertNull(impl.retrieveSecretForAudience(null));

    }

    @Test
    public void setPreferencesAndRetrieveAudienceNamespaceExceptionTest() {
        OperationalPreferencesService service = new OperationalPreferencesService() {
            @Override
            public boolean getBooleanValue(String s, String s1) throws PreferenceException, NamespaceException {
                return false;
            }

            @Override
            public int getIntegerValue(String s, String s1) throws PreferenceException, NamespaceException {
                return 0;
            }

            @Override
            public String getStringValue(String s, String s1) throws PreferenceException, NamespaceException {
                throw new NamespaceException();
            }
        };
        impl.setPreferencesService(service);
        assertNull(impl.retrieveSecretForAudience(null));

    }



}
