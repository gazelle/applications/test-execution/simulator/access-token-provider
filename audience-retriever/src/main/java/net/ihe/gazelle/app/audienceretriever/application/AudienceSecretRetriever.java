package net.ihe.gazelle.app.audienceretriever.application;

/**
 * classe to retrieve the audience secret
 */
public interface AudienceSecretRetriever {

    /**
     * retrieve secret linked to an audience
     * @param audience the audience
     * @return the secret
     */
    String retrieveSecretForAudience(String audience);
}
