package net.ihe.gazelle.app.audienceretriever.adapter;

import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;

import javax.enterprise.inject.Alternative;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * {@link AudienceSecretRetriever} used by SoapUI project to retrieve secrets.
 */
@Alternative
public class AudienceSecretRetrieverForSoapui implements AudienceSecretRetriever {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(AudienceSecretRetrieverForSoapui.class);

    private String propertiesFile;

    /**
     * Default empty construction for injection and alternative
     */
    public AudienceSecretRetrieverForSoapui() {
        //Empty constructor for injection
    }

    /**
     * Constructor allowing to configure the properties file path.
     *
     * @param propertiesFile path to the properties file.
     */
    public AudienceSecretRetrieverForSoapui(String propertiesFile) {
        this.propertiesFile = propertiesFile;
    }

    /**
     * Read property file as {@link Properties}.
     *
     * @param filePath path to hte properties file.
     * @return the {@link Properties} defined by the file.
     */
    private static Properties readPropertiesFile(String filePath) {
        Properties prop = null;
        try (FileInputStream fis = new FileInputStream(filePath)) {
            prop = new Properties();
            prop.load(fis);
        } catch (IOException e) {
            LOGGER.error("Error reading properties file !", e);
        }
        return prop;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String retrieveSecretForAudience(String audience) {
        Properties prop = readPropertiesFile(propertiesFile);
        return prop != null ? prop.getProperty(audience) : null;
    }
}
