package net.ihe.gazelle.app.audienceretriever.adapter;

import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;

import javax.enterprise.inject.Default;
import javax.inject.Inject;

/**
 * AudienceSecretRetriever implementation
 */
@Default
public class AudienceSecretRetrieverImpl implements AudienceSecretRetriever {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(AudienceSecretRetrieverImpl.class);
    private static final String AUDIENCE_JNDI_NAMESPACE = "java:/app/gazelle/audience-retriever/operational-preferences";

    @Inject
    private OperationalPreferencesService preferencesService;

    /**
     * {@inheritDoc}
     */
    public AudienceSecretRetrieverImpl() {
        //Empty constructor for injection
    }

    /**
     * Setter for the preferencesService property.
     *
     * @param preferencesService value to set to the property.
     */
    public void setPreferencesService(OperationalPreferencesService preferencesService) {
        this.preferencesService = preferencesService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String retrieveSecretForAudience(String audience) {
        try {
            return preferencesService.getStringValue(AUDIENCE_JNDI_NAMESPACE, audience);
        } catch (NamespaceException e) {
            LOGGER.warn(e, "The JNDI namespace is not configured for Audiences !");
        } catch (PreferenceException e) {
            LOGGER.warn(e, String.format("The Audience [%s] is not correctly defined  in namespace !", audience));

        }
        return null;
    }
}
