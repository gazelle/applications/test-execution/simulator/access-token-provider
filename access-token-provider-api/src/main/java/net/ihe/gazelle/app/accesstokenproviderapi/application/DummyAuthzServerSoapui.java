package net.ihe.gazelle.app.accesstokenproviderapi.application;

import net.ihe.gazelle.app.accesstokenproviderapi.business.AccessTokenRequest;
import net.ihe.gazelle.app.accesstokenproviderapi.business.SymmetricSignature;
import net.ihe.gazelle.app.audienceretriever.adapter.AudienceSecretRetrieverForSoapui;
import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.modelapi.sb.business.EncodingException;
import net.ihe.gazelle.sb.iua.business.TokenType;

import java.time.Duration;

/**
 * Dummy soapui authorization server
 */
public class DummyAuthzServerSoapui implements DummyAuthzServer {

    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(DummyAuthzServerSoapui.class);

    private static final String ALGORITHM = "HS256";
    private static final String ISSUER = "https://ehealthsuisse.ihe-europe.net/access-token-provider";
    private static final TokenType TOKEN_TYPE = TokenType.JWT;
    private static final Duration DURATION = Duration.ofHours(1);
    private AudienceSecretRetriever audienceSecretRetriever;

    /**
     * Default constructor for the class.
     */
    public DummyAuthzServerSoapui() {
        //Empty
    }

    /**
     * Constructor with the path for the class.
     */
    public DummyAuthzServerSoapui(String path) {
        audienceSecretRetriever = new AudienceSecretRetrieverForSoapui(path);
    }

    /**
     * Setter for the audienceSecretRetriever property.
     *
     * @param audienceSecretRetriever value to set to the property.
     */
    public void setAudienceSecretRetriever(AudienceSecretRetriever audienceSecretRetriever) {
        this.audienceSecretRetriever = audienceSecretRetriever;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] getAccessToken(String userId, String audienceId, String purposeOfUse, String resourceId) {
        //todo purposeOfUse and resourceId are not yet implemented

        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(this.audienceSecretRetriever);

        return getTokenGenerator(userId, audienceId, tokenGenerator);
    }


    /**
     * get the access token
     *
     * @param userId     String parameter
     * @param audienceId String parameter
     * @return AccessTokenRequest Element
     */
    public AccessTokenRequest getAccessTokenRequest(String userId, String audienceId){
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, userId, audienceId, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));
        return accessTokenRequest;
    }


    /**
     * get the generated token
     *
     * @param userId         String element
     * @param audienceId     String element
     * @param tokenGenerator TokenGenerator object
     * @return The token as byte
     */
    public byte[] getTokenGenerator(String userId, String audienceId, TokenGenerator tokenGenerator){
        byte[] token = null;
        try {
            token = tokenGenerator.generateAccessToken(getAccessTokenRequest(userId, audienceId)).getToken();
        } catch (EncodingException | TokenRequestException e) {
            LOGGER.error("Error generating Access Token", e);
        }
        return token;
    }


}
