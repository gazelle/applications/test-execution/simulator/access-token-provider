package net.ihe.gazelle.app.accesstokenproviderapi.business;


/**
 * Signature information of the access token
 */
public abstract class Signature {

    private String algorithm;

    /**
     * constructor
     */
    public Signature(String algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * get the algorithm
     * @return algorithm
     */
    public String getAlgorithm() {
        return algorithm;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Signature signature = (Signature) o;

        return algorithm.equals(signature.algorithm);
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return algorithm.hashCode();
    }
}
