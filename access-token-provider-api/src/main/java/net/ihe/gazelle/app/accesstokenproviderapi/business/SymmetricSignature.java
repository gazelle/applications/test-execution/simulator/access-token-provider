package net.ihe.gazelle.app.accesstokenproviderapi.business;

/**
 * Symmetric signature information of the access token
 */
public class SymmetricSignature extends Signature {

    private String secret;

    /**
     * constructor
     */
    public SymmetricSignature(String algorithm, String secret) {
        super(algorithm);
        this.secret = secret;
    }

    /**
     * get the secret
     * @return secret
     */
    public String getSecret() {
        return secret;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SymmetricSignature that = (SymmetricSignature) o;

        return secret.equals(that.secret);
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return secret.hashCode();
    }
}
