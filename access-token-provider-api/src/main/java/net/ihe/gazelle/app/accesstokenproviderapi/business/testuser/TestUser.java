package net.ihe.gazelle.app.accesstokenproviderapi.business.testuser;

import java.util.*;

/**
 * Test user used for authentication and token content
 */
public class TestUser {

    private String userId;
    private List<String> givenNames = new ArrayList<>();
    private String lastName;
    private Date birthDate;
    private String gender;
    private Map<String, String> extensions = new HashMap<>();

    /**
     * Constructor
     */
    public TestUser(String userId, List<String> givenNames, String lastName) {
        this.userId = userId;
        this.givenNames = givenNames;
        this.lastName = lastName;
    }

    /**
     * get the userId
     * @return userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * get the givenNames
     * @return givenNames
     */
    public List<String> getGivenNames() {
        return givenNames;
    }

    /**
     * get the lastName
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * get the birthDate
     * @return birthDate
     */
    public Date getBirthDate() {
        return (Date) birthDate.clone();
    }

    /**
     * set the birthDate
     *
     * @param birthDate the birthDate
     */
    public void setBirthDate(Date birthDate) {
        this.birthDate = (Date) birthDate.clone();
    }

    /**
     * get the gender
     * @return gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * set the gender
     *
     * @param gender the gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * get the extensions
     * @return extensions
     */
    public Map<String, String> getExtensions() {
        return extensions;
    }

    /**
     * add an extension in the extensions map
     * @param key key of the extension
     * @param value value of the extension
     */
    public void addExtension(String key, String value) {
        extensions.put(key, value);
    }

    /**
     * remove an extension in the extensions map
     * @param key key of the extension
     */
    public void removeExtension(String key) {
        extensions.remove(key);
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestUser testUser = (TestUser) o;

        if (!userId.equals(testUser.userId)) return false;
        if (!givenNames.equals(testUser.givenNames)) return false;
        if (!lastName.equals(testUser.lastName)) return false;
        if (birthDate != null ? !birthDate.equals(testUser.birthDate) : testUser.birthDate != null) return false;
        if (gender != null ? !gender.equals(testUser.gender) : testUser.gender != null) return false;
        return extensions != null ? extensions.equals(testUser.extensions) : testUser.extensions == null;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        int result = userId.hashCode();
        result = 31 * result + givenNames.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (extensions != null ? extensions.hashCode() : 0);
        return result;
    }
}
