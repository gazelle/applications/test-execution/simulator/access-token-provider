package net.ihe.gazelle.app.accesstokenproviderapi.business;

import java.util.Arrays;

/**
 * A public key
 */
public class PublicKey implements Credential {
    private byte[] key;

    /**
     * constructor
     */
    public PublicKey(byte[] key) {
        this.key = key.clone();
    }

    /**
     * get the key
     * @return key
     */
    public byte[] getKey() {
        return key.clone();
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PublicKey publicKey = (PublicKey) o;

        return Arrays.equals(key, publicKey.key);
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return Arrays.hashCode(key);
    }
}
