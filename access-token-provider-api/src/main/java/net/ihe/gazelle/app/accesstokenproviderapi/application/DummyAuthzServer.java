package net.ihe.gazelle.app.accesstokenproviderapi.application;

/**
 * For SoapUI integration need, a simplified Authorization Server (or IDP) is required.
 */
public interface DummyAuthzServer {

    /**
     * get a dummy access token
     * @param userId
     * @param audienceId
     * @param purposeOfUse
     * @param resourceId
     * @return an access token
     */
    byte[] getAccessToken(String userId, String audienceId, String purposeOfUse, String resourceId);

}
