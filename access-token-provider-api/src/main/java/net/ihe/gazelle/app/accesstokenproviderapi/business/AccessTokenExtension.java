package net.ihe.gazelle.app.accesstokenproviderapi.business;

import java.util.ArrayList;
import java.util.List;

/**
 * Extensions for the Access Token
 */
public class AccessTokenExtension {

    private String subjectId;
    private List<String> subjectOrganizations = new ArrayList<>();
    private List<String> subjectOrganizationIds = new ArrayList<>();
    private String homeCommunityId;
    private String nationalProviderIdentifier;
    private List<String> providerIds = new ArrayList<>();
    private CodedValue subjectRole;
    private CodedValue purposeOfUse;
    private String resourceId;
    private String onBehalfOf;

    /**
     * constructor
     */
    public AccessTokenExtension() {
        // Constructor is empty because all variables are optionals.
    }

    /**
     * get the subjectId
     * @return subjectId
     */
    public String getSubjectId() {
        return subjectId;
    }

    /**
     * set the subjectId
     * @param subjectId the subjectId
     */
    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    /**
     * get subjectOrganizations list
     * @return subjectOrganizations
     */
    public List<String> getSubjectOrganizations() {
        return subjectOrganizations;
    }

    /**
     * add a subjectOrganization in the subjectOrganizations list
     * @param subjectOrganization a nationalProviderIdentifier
     */
    public void addSubjectOrganization(String subjectOrganization) {
        this.subjectOrganizations.add(subjectOrganization);
    }

    /**
     * remove a subjectOrganization from the subjectOrganizations list
     * @param subjectOrganization a nationalProviderIdentifier
     */
    public void removeSubjectOrganization(String subjectOrganization) {
        this.subjectOrganizations.remove(subjectOrganization);
    }

    /**
     * get subjectOrganizationIds list
     * @return subjectOrganizationIds
     */
    public List<String> getSubjectOrganizationIds() {
        return subjectOrganizationIds;
    }

    /**
     * add a subjectOrganizationId in the subjectOrganizationIds list
     * @param subjectOrganizationId a subjectOrganizationId
     */
    public void addSubjectOrganizationId(String subjectOrganizationId) {
        this.subjectOrganizationIds.add(subjectOrganizationId);
    }

    /**
     * remove a subjectOrganizationId from the subjectOrganizationIds list
     * @param subjectOrganizationId a subjectOrganizationId
     */
    public void removeSubjectOrganizationId(String subjectOrganizationId) {
        this.subjectOrganizationIds.remove(subjectOrganizationId);
    }

    /**
     * get the homeCommunityId
     * @return homeCommunityId
     */
    public String getHomeCommunityId() {
        return homeCommunityId;
    }

    /**
     * set the homeCommunityId
     * @param homeCommunityId the homeCommunityId
     */
    public void setHomeCommunityId(String homeCommunityId) {
        this.homeCommunityId = homeCommunityId;
    }

    /**
     * get the nationalProviderIdentifier
     * @return nationalProviderIdentifier
     */
    public String getNationalProviderIdentifier() {
        return nationalProviderIdentifier;
    }

    /**
     * set the nationalProviderIdentifier
     * @param nationalProviderIdentifier the nationalProviderIdentifier
     */
    public void setNationalProviderIdentifier(String nationalProviderIdentifier) {
        this.nationalProviderIdentifier = nationalProviderIdentifier;
    }

    /**
     * get providerIds list
     * @return providerIds
     */
    public List<String> getProviderIds() {
        return providerIds;
    }

    /**
     * add a providerId in the providerIds list
     * @param providerId a providerId
     */
    public void addProviderId(String providerId) {
        this.providerIds.add(providerId);
    }

    /**
     * remove a providerId in the providerIds list
     * @param providerId a providerId
     */
    public void removeProviderId(String providerId) {
        this.providerIds.remove(providerId);
    }

    /**
     * get the subjectRole
     * @return subjectRole
     */
    public CodedValue getSubjectRole() {
        return subjectRole;
    }

    /**
     * set the subjectRole
     * @param subjectRole the subjectRole
     */
    public void setSubjectRole(CodedValue subjectRole) {
        this.subjectRole = subjectRole;
    }

    /**
     * get the purposeOfUse
     * @return purposeOfUse
     */
    public CodedValue getPurposeOfUse() {
        return purposeOfUse;
    }

    /**
     * set the purposeOfUse
     * @param purposeOfUse the purposeOfUse
     */
    public void setPurposeOfUse(CodedValue purposeOfUse) {
        this.purposeOfUse = purposeOfUse;
    }

    /**
     * get the resourceId
     * @return resourceId
     */
    public String getResourceId() {
        return resourceId;
    }

    /**
     * set the resourceId
     * @param resourceId the resourceId
     */
    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * get the onBehalfOf
     * @return onBehalfOf
     */
    public String getOnBehalfOf() {
        return onBehalfOf;
    }

    /**
     * set the onBehalfOf
     * @param onBehalfOf the onBehalfOf
     */
    public void setOnBehalfOf(String onBehalfOf) {
        this.onBehalfOf = onBehalfOf;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessTokenExtension that = (AccessTokenExtension) o;

        if (subjectId != null ? !subjectId.equals(that.subjectId) : that.subjectId != null) return false;
        if (subjectOrganizations != null ? !subjectOrganizations.equals(that.subjectOrganizations) : that.subjectOrganizations != null)
            return false;
        if (subjectOrganizationIds != null ? !subjectOrganizationIds.equals(that.subjectOrganizationIds) : that.subjectOrganizationIds != null)
            return false;
        if (homeCommunityId != null ? !homeCommunityId.equals(that.homeCommunityId) : that.homeCommunityId != null)
            return false;
        if (nationalProviderIdentifier != null ? !nationalProviderIdentifier.equals(that.nationalProviderIdentifier) : that.nationalProviderIdentifier != null)
            return false;
        if (providerIds != null ? !providerIds.equals(that.providerIds) : that.providerIds != null) return false;
        if (subjectRole != null ? !subjectRole.equals(that.subjectRole) : that.subjectRole != null) return false;
        if (purposeOfUse != null ? !purposeOfUse.equals(that.purposeOfUse) : that.purposeOfUse != null) return false;
        if (resourceId != null ? !resourceId.equals(that.resourceId) : that.resourceId != null) return false;
        return onBehalfOf != null ? onBehalfOf.equals(that.onBehalfOf) : that.onBehalfOf == null;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        int result = subjectId != null ? subjectId.hashCode() : 0;
        result = 31 * result + (subjectOrganizations != null ? subjectOrganizations.hashCode() : 0);
        result = 31 * result + (subjectOrganizationIds != null ? subjectOrganizationIds.hashCode() : 0);
        result = 31 * result + (homeCommunityId != null ? homeCommunityId.hashCode() : 0);
        result = 31 * result + (nationalProviderIdentifier != null ? nationalProviderIdentifier.hashCode() : 0);
        result = 31 * result + (providerIds != null ? providerIds.hashCode() : 0);
        result = 31 * result + (subjectRole != null ? subjectRole.hashCode() : 0);
        result = 31 * result + (purposeOfUse != null ? purposeOfUse.hashCode() : 0);
        result = 31 * result + (resourceId != null ? resourceId.hashCode() : 0);
        result = 31 * result + (onBehalfOf != null ? onBehalfOf.hashCode() : 0);
        return result;
    }
}
