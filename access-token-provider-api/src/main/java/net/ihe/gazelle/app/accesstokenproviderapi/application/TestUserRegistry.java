package net.ihe.gazelle.app.accesstokenproviderapi.application;

import net.ihe.gazelle.app.accesstokenproviderapi.business.testuser.TestUser;

/**
 * Interface to interact with the test-users’ database for authentication step and token content
 */
public interface TestUserRegistry {

    /**
     * @param userId
     * @return TestUser
     */
    TestUser getTestUser(String userId);

}
