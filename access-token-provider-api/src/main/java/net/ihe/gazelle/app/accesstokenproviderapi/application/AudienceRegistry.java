package net.ihe.gazelle.app.accesstokenproviderapi.application;

import net.ihe.gazelle.app.accesstokenproviderapi.business.Credential;

/**
 * Interface to interact with the audience registry where is store all known audience with their credential
 */
public interface AudienceRegistry {

    /**
     * Get credential's audience
     * @param audienceId
     * @return credential
     */
    Credential getAudienceCredentials(String audienceId);

}
