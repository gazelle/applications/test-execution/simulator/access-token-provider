package net.ihe.gazelle.app.accesstokenproviderapi.application;

import net.ihe.gazelle.app.accesstokenproviderapi.business.AccessTokenRequest;
import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;
import net.ihe.gazelle.modelapi.sb.business.EncodingException;
import net.ihe.gazelle.sb.iua.business.EncodedIUAToken;
import net.ihe.gazelle.sb.iua.business.TokenType;
import net.ihe.gazelle.sb.jwtstandardblock.adapter.JJWTAdapter;
import net.ihe.gazelle.sb.jwtstandardblock.application.JWSEncoderDecoder;
import net.ihe.gazelle.sb.jwtstandardblock.business.jose.JOSEHeader;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.JSONWebKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.KeyAlgorithm;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwk.SymmetricalKey;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebSignature;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebToken;
import net.ihe.gazelle.sb.jwtstandardblock.business.jwt.JSONWebTokenClaimSet;

import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Class to generate the token
 */
public class TokenGenerator {

    private static final String ALGORITHM = "HS256";
    private static final String ISSUER = "https://ehealthsuisse.ihe-europe.net/access-token-provider";
    private static final TokenType TOKEN_TYPE = TokenType.JWT;
    private static final Duration DEFAULT_DURATION = Duration.ofMinutes(5);
    private static final List<String> SUBJECTS = Arrays.asList("aamrein", "aerne", "aerne2", "bovie", "lavdic", "magpar", "rspieler", "aandrews",
            "ltieche");

    private AudienceSecretRetriever audienceSecretRetriever;

    /**
     * Set an audience secret
     *
     * @param audienceSecretRetriever AudienceSecretRetriever element
     */
    public void setAudienceSecretRetriever(AudienceSecretRetriever audienceSecretRetriever) {
        this.audienceSecretRetriever = audienceSecretRetriever;
    }

    /**
     * Encode the IUA token
     *
     * @param accessTokenRequest AccessTokenRequest element
     * @return The EncodedIUAToken
     * @throws EncodingException
     * @throws TokenRequestException
     */
    public EncodedIUAToken generateAccessToken(AccessTokenRequest accessTokenRequest) throws EncodingException, TokenRequestException {
        if (accessTokenRequest.getSignature() == null) {
            throw new TokenRequestException("Missing signature information");
        }

        if (accessTokenRequest.getSignature().getAlgorithm() == null || !accessTokenRequest.getSignature().getAlgorithm().equals(ALGORITHM)) {
            throw new TokenRequestException("Unsupported Algorithm");
        }

        Duration duration = accessTokenRequest.getValidityTime() != null ? accessTokenRequest.getValidityTime() : DEFAULT_DURATION;

        if (accessTokenRequest.getTokenType() == null || !accessTokenRequest.getTokenType().equals(TOKEN_TYPE)) {
            throw new TokenRequestException("Unsupported token type");
        }

        if (accessTokenRequest.getIssuer() == null || !accessTokenRequest.getIssuer().equals(ISSUER)) {
            throw new TokenRequestException("Unsupported issuer");
        }

        if (accessTokenRequest.getAudience() == null || accessTokenRequest.getAudience().isEmpty()) {
            throw new TokenRequestException("Audience is null or empty");
        }

        if (accessTokenRequest.getSubject() == null || !SUBJECTS.contains(accessTokenRequest.getSubject())) {
            throw new TokenRequestException("Unsupported subject");
        }

        String secret = audienceSecretRetriever.retrieveSecretForAudience(accessTokenRequest.getAudience());
        if (secret == null || secret.isEmpty()) {
            throw new TokenRequestException("Audience is not known");
        }

        JSONWebTokenClaimSet claimSet = new JSONWebTokenClaimSet();

        claimSet.setSubject(accessTokenRequest.getSubject());
        claimSet.setIssuer(ISSUER);
        claimSet.setAudience(accessTokenRequest.getAudience());

        ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
        claimSet.setIssuedAt(new Date(now.toEpochSecond()*1000));
        claimSet.setExpiration(new Date(now.plus(duration).toEpochSecond()*1000));

        claimSet.setJwtId(UUID.randomUUID().toString());

        JOSEHeader joseHeader = new JOSEHeader(false, null, KeyAlgorithm.HS256);
        JSONWebKey jsonWebKey = new SymmetricalKey(secret, null, KeyAlgorithm.HS256);
        JSONWebSignature jose = new JSONWebSignature(jsonWebKey, joseHeader);

        JSONWebToken token = new JSONWebToken(UUID.randomUUID().toString(), "IUA", jose, claimSet); //Verify standard keyword

        JWSEncoderDecoder jwsEncoderDecoder = new JWSEncoderDecoder(new JJWTAdapter());

        EncodedIUAToken encodedIUAToken = new EncodedIUAToken(jwsEncoderDecoder.encode(token).getCompletePayload().getBytes(StandardCharsets.UTF_8));
        encodedIUAToken.setTokenType(TokenType.JWT);

        return encodedIUAToken;
    }


}
