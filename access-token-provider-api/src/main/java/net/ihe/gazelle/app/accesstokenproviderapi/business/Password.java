package net.ihe.gazelle.app.accesstokenproviderapi.business;

import java.util.Arrays;

/**
 * A password
 */
public class Password implements Credential {

    private byte[] value;

    /**
     * constructor
     */
    public Password(byte[] value) {
        this.value = value.clone();
    }

    /**
     * get the value
     * @return value
     */
    public byte[] getValue() {
        return value.clone();
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Password password = (Password) o;

        return Arrays.equals(value, password.value);
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        return Arrays.hashCode(value);
    }
}
