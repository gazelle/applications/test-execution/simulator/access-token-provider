package net.ihe.gazelle.app.accesstokenproviderapi.business;

import net.ihe.gazelle.sb.iua.business.TokenType;

import java.time.Duration;

/**
 * The Access Token request
 */
public class AccessTokenRequest {

    private String issuer;
    private String subject;
    private String audience;
    private Duration validityTime;
    private TokenType tokenType;
    private Signature signature;
    private AccessTokenExtension extension;

    /**
     * constructor
     */
    public AccessTokenRequest(String issuer, String subject, String audience, Duration validityTime, TokenType tokenType) {
        this.issuer = issuer;
        this.subject = subject;
        this.audience = audience;
        this.validityTime = validityTime;
        this.tokenType = tokenType;
    }

    /**
     * get the issuer
     *
     * @return issuer
     */
    public String getIssuer() {
        return issuer;
    }

    /**
     * get the subject
     *
     * @return subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * get the audience
     *
     * @return audience
     */
    public String getAudience() {
        return audience;
    }

    /**
     * get the validityTime
     *
     * @return validityTime
     */
    public Duration getValidityTime() {
        return validityTime;
    }

    /**
     * get the tokenType
     *
     * @return tokenType
     */
    public TokenType getTokenType() {
        return tokenType;
    }

    /**
     * get the signature
     *
     * @return signature
     */
    public Signature getSignature() {
        return signature;
    }

    /**
     * set the signature
     *
     * @param signature the signature
     */
    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    /**
     * get the extension
     *
     * @return extension
     */
    public AccessTokenExtension getExtension() {
        return extension;
    }

    /**
     * set the extension
     *
     * @param extension the extension
     */
    public void setExtension(AccessTokenExtension extension) {
        this.extension = extension;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccessTokenRequest that = (AccessTokenRequest) o;

        if (!issuer.equals(that.issuer)) return false;
        if (!subject.equals(that.subject)) return false;
        if (!audience.equals(that.audience)) return false;
        if (!validityTime.equals(that.validityTime)) return false;
        if (tokenType != that.tokenType) return false;
        if (signature != null ? !signature.equals(that.signature) : that.signature != null) return false;
        return extension != null ? extension.equals(that.extension) : that.extension == null;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        int result = issuer.hashCode();
        result = 31 * result + subject.hashCode();
        result = 31 * result + audience.hashCode();
        result = 31 * result + validityTime.hashCode();
        result = 31 * result + tokenType.hashCode();
        result = 31 * result + (signature != null ? signature.hashCode() : 0);
        result = 31 * result + (extension != null ? extension.hashCode() : 0);
        return result;
    }
}
