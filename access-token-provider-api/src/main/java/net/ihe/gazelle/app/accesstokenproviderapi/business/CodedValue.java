package net.ihe.gazelle.app.accesstokenproviderapi.business;

/**
 * A Coded value
 */
public class CodedValue {

    private String code;
    private String codeSystem;
    private String codeSystemName;
    private String displayName;

    /**
     * constructor
     */
    public CodedValue(String code, String codeSystem) {
        this.code = code;
        this.codeSystem = codeSystem;
    }

    /**
     * get the code
     *
     * @return code
     */
    public String getCode() {
        return code;
    }

    /**
     * get the codeSystem
     *
     * @return codeSystem
     */
    public String getCodeSystem() {
        return codeSystem;
    }

    /**
     * get the codeSystemName
     *
     * @return codeSystemName
     */
    public String getCodeSystemName() {
        return codeSystemName;
    }

    /**
     * set the codeSystemName
     *
     * @param codeSystemName the codeSystemName
     */
    public void setCodeSystemName(String codeSystemName) {
        this.codeSystemName = codeSystemName;
    }

    /**
     * get the displayName
     *
     * @return displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * set the displayName
     *
     * @param displayName the displayName
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
