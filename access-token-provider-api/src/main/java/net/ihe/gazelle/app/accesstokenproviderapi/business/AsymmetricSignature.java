package net.ihe.gazelle.app.accesstokenproviderapi.business;

import java.util.Arrays;

/**
 * Asymmetric signature information of the access token
 */
public class AsymmetricSignature extends Signature {

    private byte[] privateKey;
    private String privateKeyPassword;

    /**
     * constructor
     */
    public AsymmetricSignature(String algorithm, byte[] privateKey, String privateKeyPassword) {
        super(algorithm);
        this.privateKey = privateKey.clone();
        this.privateKeyPassword = privateKeyPassword;
    }

    /**
     * get the privateKey
     * @return privateKey
     */
    public byte[] getPrivateKey() {
        return privateKey.clone();
    }

    /**
     * get the privateKeyPassword
     * @return privateKeyPassword
     */
    public String getPrivateKeyPassword() {
        return privateKeyPassword;
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AsymmetricSignature that = (AsymmetricSignature) o;

        if (!Arrays.equals(privateKey, that.privateKey)) return false;
        return privateKeyPassword.equals(that.privateKeyPassword);
    }

    @Override
    /**
     * {@inheritDoc}
     */
    public int hashCode() {
        int result = Arrays.hashCode(privateKey);
        result = 31 * result + privateKeyPassword.hashCode();
        return result;
    }
}
