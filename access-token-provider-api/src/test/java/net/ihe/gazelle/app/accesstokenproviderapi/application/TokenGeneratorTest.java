package net.ihe.gazelle.app.accesstokenproviderapi.application;

import net.ihe.gazelle.app.accesstokenproviderapi.business.AccessTokenRequest;
import net.ihe.gazelle.app.accesstokenproviderapi.business.SymmetricSignature;
import net.ihe.gazelle.modelapi.sb.business.EncodingException;
import net.ihe.gazelle.sb.iua.business.EncodedIUAToken;
import net.ihe.gazelle.sb.iua.business.TokenType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for {@link TokenGenerator}
 */
class TokenGeneratorTest {

    private static final String ALGORITHM = "HS256";
    private static final String SUBJECT = "aamrein";
    private static final String AUDIENCE = "audience";
    private static final String ISSUER = "https://ehealthsuisse.ihe-europe.net/access-token-provider";
    private static final TokenType TOKEN_TYPE = TokenType.JWT;
    private static final Duration DURATION = Duration.ofMinutes(5);
    private static AudienceSecretRetrieverTestImpl AUDIENCE_RETRIEVER = new AudienceSecretRetrieverTestImpl();

    /**
     * Init audience available to generate token.
     */
    @BeforeEach
    public void initAudience() {
        AUDIENCE_RETRIEVER.addAudience(AUDIENCE, "myBeautifulKeyWhichIsAJWTSecretSoSecret");
    }

    /**
     * Default generation of a token
     *
     * @throws EncodingException     if something wrong happens during encoding
     * @throws TokenRequestException if the token request is not valid
     */
    @Test
    public void generateAccessTokenTest() throws EncodingException, TokenRequestException {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "myBeautifulKeyWhichIsAJWTSecretSoSecret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        EncodedIUAToken encodedIUAToken = tokenGenerator.generateAccessToken(accessTokenRequest);
        assertNotNull(encodedIUAToken);
    }

    /**
     * Test with a null signature.
     */
    @Test
    public void generateAccessTokenNullSignatureTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(null, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(null);


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating token with unsupported issuer.
     */
    @Test
    public void generateAccessTokenUnsupportedIssuerTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest("blabla", SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with a null subject.
     */
    @Test
    public void generateAccessTokenNullIssuerTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(null, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with unsupported subject.
     */
    @Test
    public void generateAccessTokenUnsupportedSubjectTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, "subject", AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with null subject.
     */
    @Test
    public void generateAccessTokenNullSubjectTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, null, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with unsupported token type.
     */
    @Test
    public void generateAccessTokenUnsupportedTokenTypeTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TokenType.SAML);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with null token type.
     */
    @Test
    public void generateAccessTokenNullTokenTypeTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, null);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with unsupported algo.
     */
    @Test
    public void generateAccessTokenUnsupportedAlgoTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature("algo", "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with null algo.
     */
    @Test
    public void generateAccessTokenNullAlgoTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(null, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with null audience.
     */
    @Test
    public void generateAccessTokenNullAudienceTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, null, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with unsupported audience.
     */
    @Test
    public void generateAccessTokenUnknownAudienceTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, "pouet", DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with empty audience.
     */
    @Test
    public void generateAccessTokenEmptyAudienceTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, "", DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(AUDIENCE_RETRIEVER);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with empty secret.
     */
    @Test
    public void generateAccessTokenEmptySecretTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, "pouet", DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(audience -> "");

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with null secret.
     */
    @Test
    public void generateAccessTokenNullSecretTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, "pouet", DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(audience -> null);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }

    /**
     * Test generating a token with null Duration.
     */
    @Test
    public void generateAccessTokenDurationNullTest() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, "pouet", null, TOKEN_TYPE);
        accessTokenRequest.setSignature(new SymmetricSignature(ALGORITHM, "secret"));


        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.setAudienceSecretRetriever(audience -> null);

        assertThrows(TokenRequestException.class, () -> tokenGenerator.generateAccessToken(accessTokenRequest), "Unsupported issuer");
    }
}
