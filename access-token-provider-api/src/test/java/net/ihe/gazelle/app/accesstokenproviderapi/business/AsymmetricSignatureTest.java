package net.ihe.gazelle.app.accesstokenproviderapi.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AsymmetricSignatureTest {

    private static final byte[] KEY = "key".getBytes();
    private static final String PASSWORD = "password";
    private static final String ALGORITHM = "RSAPOUETPOUET";

    /**
     * Test privateKey getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-010")
    void getPrivateKey(){
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        assertArrayEquals(KEY, signature.getPrivateKey(), "Getter shall return the value of privateKey !");
    }

    /**
     * Test privateKeyPassword getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-011")
    void getPrivateKeyPassword(){
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        assertEquals(PASSWORD, signature.getPrivateKeyPassword(), "Getter shall return the value of privateKeyPassword !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        assertFalse(signature.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        assertTrue(signature.equals(signature), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        assertFalse(signature.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different algorithm
     */
    @Test
    void equalsDifferentAlgorithm() {
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        AsymmetricSignature signature2 = new AsymmetricSignature("ALGORITHM", KEY, PASSWORD);
        assertFalse(signature.equals(signature2), "Objects shall not be equal if they have different algorithm !");
    }

    /**
     * Test equals with different privateKey
     */
    @Test
    void equalsDifferentPrivateKey() {
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        AsymmetricSignature signature2 = new AsymmetricSignature(ALGORITHM, "KEY".getBytes(), PASSWORD);
        assertFalse(signature.equals(signature2), "Objects shall not be equal if they have different privateKey !");
    }

    /**
     * Test equals with different privateKeyPassword
     */
    @Test
    void equalsDifferentPrivateKeyPassword() {
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        AsymmetricSignature signature2 = new AsymmetricSignature(ALGORITHM, KEY, "PASSWORD");
        assertFalse(signature.equals(signature2), "Objects shall not be equal if they have different privateKeyPassword !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        AsymmetricSignature signature = new AsymmetricSignature(ALGORITHM, KEY, PASSWORD);
        assertNotNull(signature.hashCode(), "Generated hashCode shall not be null !");
    }


}
