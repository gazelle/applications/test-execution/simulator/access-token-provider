package net.ihe.gazelle.app.accesstokenproviderapi.business.testuser;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TestUserTest {

    private static final String USER_ID = "id";
    private static final String GIVEN_NAME = "given";
    private static final String LAST_NAME = "lastName";
    private static final Date BIRTH_DATE = new Date();
    private static final String GENDER = "gender";

    /**
     * Test userId getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-023")
    void getUserId() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        assertEquals(USER_ID, testUser.getUserId(), "Getter shall return the value of userId !");
    }

    /**
     * Test givenNames getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-024")
    void getGivenNames() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        assertEquals(givenNames, testUser.getGivenNames(), "Getter shall return the value of givenNames !");
    }

    /**
     * Test lastName getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-025")
    void getlastName() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        assertEquals(LAST_NAME, testUser.getLastName(), "Getter shall return the value of lastName !");
    }

    /**
     * Test birthDate getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-026")
    void birthDate() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser.setBirthDate(BIRTH_DATE);
        assertEquals(BIRTH_DATE, testUser.getBirthDate(), "Getter shall return the value of birthDate !");
    }

    /**
     * Test birthDate getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-027")
    void gender() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser.setGender(GENDER);
        assertEquals(GENDER, testUser.getGender(), "Getter shall return the value of gender !");
    }

    /**
     * Test addExtension and removeExtension.
     */
    @Test
    @Covers(requirements = "TOKENPROV-028")
    void addExtension() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser.addExtension("key1", "value1");
        assertEquals(1, testUser.getExtensions().size());
        testUser.addExtension("key2", "value2");
        assertEquals(2, testUser.getExtensions().size());
        testUser.removeExtension("key1");
        assertEquals(1, testUser.getExtensions().size());
        assertEquals("value2", testUser.getExtensions().get("key2"));
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        assertFalse(testUser.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        assertTrue(testUser.equals(testUser), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        assertFalse(testUser.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different issuerId
     */
    @Test
    void equalsDifferentUserID() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        TestUser testUser2 = new TestUser("USER_ID", givenNames, LAST_NAME);
        assertFalse(testUser.equals(testUser2), "Objects shall not be equal if they have different issuerId !");
    }

    /**
     * Test equals with different givenNames
     */
    @Test
    void equalsDifferentGivenNames() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        List<String> givenNames2 = new ArrayList<>();
        givenNames2.add("given2");
        TestUser testUser2 = new TestUser(USER_ID, givenNames2, LAST_NAME);
        assertFalse(testUser.equals(testUser2), "Objects shall not be equal if they have different givenNames !");
    }


    /**
     * Test equals with different lastName
     */
    @Test
    void equalsDifferentLastName() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        TestUser testUser2 = new TestUser(USER_ID, givenNames, "LAST_NAME");
        assertFalse(testUser.equals(testUser2), "Objects shall not be equal if they have different lastName !");
    }

    /**
     * Test equals with different birthName
     */
    @Test
    void equalsDifferentBirthName() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser.setBirthDate(BIRTH_DATE);
        TestUser testUser2 = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser2.setBirthDate(new Date());
        assertFalse(testUser.equals(testUser2), "Objects shall not be equal if they have different birthName !");
    }

    /**
     * Test equals with different gender
     */
    @Test
    void equalsDifferentGender() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser.setGender(GENDER);
        TestUser testUser2 = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser.setGender("GENDER");
        assertFalse(testUser.equals(testUser2), "Objects shall not be equal if they have different gender !");
    }

    /**
     * Test equals with different extensions
     */
    @Test
    void equalsDifferentExtensions() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser.addExtension("1", "1");
        TestUser testUser2 = new TestUser(USER_ID, givenNames, LAST_NAME);
        testUser2.addExtension("1", "1");
        testUser2.addExtension("2", "2");
        assertFalse(testUser.equals(testUser2), "Objects shall not be equal if they have different extensions !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        List<String> givenNames = new ArrayList<>();
        givenNames.add(GIVEN_NAME);
        TestUser testUser = new TestUser(USER_ID, givenNames, LAST_NAME);
        assertNotNull(testUser.hashCode(), "Generated hashCode shall not be null !");
    }

}
