package net.ihe.gazelle.app.accesstokenproviderapi.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SymmetricSignatureTest {

    private static final String ALGO = "RSAPOUETPOUET";
    private static final String SECRET = "PSST";

    /**
     * Test privateKey getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-009")
    void getPrivateKey() {
        SymmetricSignature signature = new SymmetricSignature(ALGO, SECRET);
        assertEquals(SECRET, signature.getSecret(), "Getter shall return the value of privateKey !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        SymmetricSignature signature = new SymmetricSignature(ALGO, SECRET);
        assertFalse(signature.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        SymmetricSignature signature = new SymmetricSignature(ALGO, SECRET);
        assertTrue(signature.equals(signature), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        SymmetricSignature signature = new SymmetricSignature(ALGO, SECRET);
        assertFalse(signature.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different algorithm
     */
    @Test
    void equalsDifferentAlgorithm() {
        SymmetricSignature signature = new SymmetricSignature(ALGO, SECRET);
        SymmetricSignature signature2 = new SymmetricSignature("ALGO", SECRET);
        assertFalse(signature.equals(signature2), "Objects shall not be equal if they have different algorithm !");
    }

    /**
     * Test equals with different secret
     */
    @Test
    void equalsDifferentSecret() {
        SymmetricSignature signature = new SymmetricSignature(ALGO, SECRET);
        SymmetricSignature signature2 = new SymmetricSignature(ALGO, "SECRET");
        assertFalse(signature.equals(signature2), "Objects shall not be equal if they have different secret !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        SymmetricSignature signature = new SymmetricSignature(ALGO, SECRET);
        assertNotNull(signature.hashCode(), "Generated hashCode shall not be null !");
    }

}
