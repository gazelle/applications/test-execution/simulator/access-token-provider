package net.ihe.gazelle.app.accesstokenproviderapi.application;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * Class to test the soapui authorization server
 */
class DummyAuthzServerSoapuiTest {

    private static final String SUBJECT = "aamrein";
    private static final String AUDIENCE = "audience";

    /**
     * get access token with an Audience path defined
     */
    @Test
    public void getAccessTokenWithPathAudienceTest() {
        DummyAuthzServerSoapui dummyAuthzServer = new DummyAuthzServerSoapui();
        dummyAuthzServer.setAudienceSecretRetriever((String audience) -> "myBeautifulKeyWhichIsAJWTSecretSoSecret");


        assertNotNull(dummyAuthzServer.getAccessToken(SUBJECT, AUDIENCE, null, null), "check that the access token is not null");
    }

    /**
     * get access token without an Audience path defined (we keep the default Audience path in this case)
     */
    @Test
    public void getAccessTokenWithDefaultPathAudienceTest() {
        DummyAuthzServerSoapui dummyAuthzServer = new DummyAuthzServerSoapui();
        dummyAuthzServer.setAudienceSecretRetriever((String audience) -> "myBeautifulKeyWhichIsAJWTSecretSoSecret");

        assertNotNull(dummyAuthzServer.getAccessToken(SUBJECT, AUDIENCE, null, null), "check that the access token is not null");
    }

    /**
     * get access token with an invalid audience path provide
     */
    @Test
    public void getAccessTokenWithBadPathAudienceTest() {
        DummyAuthzServerSoapui dummyAuthzServer = new DummyAuthzServerSoapui("test.properties");
        assertNull(dummyAuthzServer.getAccessToken(SUBJECT, AUDIENCE, null, null), "check that the access token is null");
    }
}
