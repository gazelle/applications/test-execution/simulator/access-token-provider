package net.ihe.gazelle.app.accesstokenproviderapi.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AccessTokenExtensionTest {

    /**
     * Test subjectId getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-012")
    void getSetSubjectId(){
        String subjectId = "id";
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setSubjectId(subjectId);
        assertEquals(subjectId, accessTokenExtension.getSubjectId(), "Getter shall return the value of subjectId !");
    }

    /**
     * Test addSubjectOrganization and removeSubjectOrganization.
     */
    @Test
    @Covers(requirements = "TOKENPROV-013")
    void subjectOrganization(){
        String orga1 = "orga1";
        String orga2 = "orga2";
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.addSubjectOrganization(orga1);
        assertEquals(1, accessTokenExtension.getSubjectOrganizations().size());
        accessTokenExtension.addSubjectOrganization(orga2);
        assertEquals(2, accessTokenExtension.getSubjectOrganizations().size());
        accessTokenExtension.removeSubjectOrganization(orga1);
        assertEquals(1, accessTokenExtension.getSubjectOrganizations().size());
        assertEquals(orga2, accessTokenExtension.getSubjectOrganizations().get(0));
    }

    /**
     * Test addSubjectOrganizationId and removeSubjectOrganizationId.
     */
    @Test
    @Covers(requirements = "TOKENPROV-014")
    void subjectOrganizationId(){
        String orgaId1 = "orgaId1";
        String orgaId2 = "orgaId2";
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.addSubjectOrganizationId(orgaId1);
        assertEquals(1, accessTokenExtension.getSubjectOrganizationIds().size());
        accessTokenExtension.addSubjectOrganizationId(orgaId2);
        assertEquals(2, accessTokenExtension.getSubjectOrganizationIds().size());
        accessTokenExtension.removeSubjectOrganizationId(orgaId1);
        assertEquals(1, accessTokenExtension.getSubjectOrganizationIds().size());
        assertEquals(orgaId2, accessTokenExtension.getSubjectOrganizationIds().get(0));
    }

    /**
     * Test subjectId getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-015")
    void homeCommunityId(){
        String homeCommunityId = "id";
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setHomeCommunityId(homeCommunityId);
        assertEquals(homeCommunityId, accessTokenExtension.getHomeCommunityId());
    }

    /**
     * Test subjectId getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-016")
    void nationalProviderIdentifier(){
        String nationalProviderIdentifier = "id";
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setNationalProviderIdentifier(nationalProviderIdentifier);
        assertEquals(nationalProviderIdentifier, accessTokenExtension.getNationalProviderIdentifier());
    }

    /**
     * Test addProviderId and removeProviderId.
     */
    @Test
    @Covers(requirements = "TOKENPROV-017")
    void providerId(){
        String providerId1 = "provId1";
        String providerId2 = "provId2";
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.addProviderId(providerId1);
        assertEquals(1, accessTokenExtension.getProviderIds().size());
        accessTokenExtension.addProviderId(providerId2);
        assertEquals(2, accessTokenExtension.getProviderIds().size());
        accessTokenExtension.removeProviderId(providerId1);
        assertEquals(1, accessTokenExtension.getProviderIds().size());
        assertEquals(providerId2, accessTokenExtension.getProviderIds().get(0));
    }

    /**
     * Test subjectRole getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-018")
    void subjectRole(){
        CodedValue codedValue = new CodedValue("a", "b");
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setSubjectRole(codedValue);
        assertEquals(codedValue, accessTokenExtension.getSubjectRole());
    }

    /**
     * Test purposeOfUse getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-019")
    void purposeOfUse(){
        CodedValue codedValue = new CodedValue("a", "b");
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setPurposeOfUse(codedValue);
        assertEquals(codedValue, accessTokenExtension.getPurposeOfUse());
    }

    /**
     * Test resourceId getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-020")
    void resourceId(){
        String resourceId = "id";
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setResourceId(resourceId);
        assertEquals(resourceId, accessTokenExtension.getResourceId());
    }

    /**
     * Test onBehalfOf getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-021")
    void onBehalfOf(){
        String onBehalfOf = "toto";
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setOnBehalfOf(onBehalfOf);
        assertEquals(onBehalfOf, accessTokenExtension.getOnBehalfOf());
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        assertFalse(accessTokenExtension.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        assertTrue(accessTokenExtension.equals(accessTokenExtension), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        assertFalse(accessTokenExtension.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different subjectId
     */
    @Test
    void equalsDifferentSubjectId() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setSubjectId("1");
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.setSubjectId("2");
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different subjectId !");
    }

    /**
     * Test equals with different subjectOrganization
     */
    @Test
    void equalsDifferentSubjectOrganization() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.addSubjectOrganization("1");
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.addSubjectOrganization("2");
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different subjectOrganization !");
    }

    /**
     * Test equals with different subjectOrganizationId
     */
    @Test
    void equalsDifferentSubjectOrganizationId() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.addSubjectOrganizationId("1");
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.addSubjectOrganizationId("2");
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different subjectOrganizationId !");
    }

    /**
     * Test equals with different homeCommunityId
     */
    @Test
    void equalsDifferentHomeCommunityId() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setHomeCommunityId("1");
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.setHomeCommunityId("2");
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different homeCommunityId !");
    }

    /**
     * Test equals with different nationalProviderIdentifier
     */
    @Test
    void equalsDifferentNationalProviderIdentifier() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setNationalProviderIdentifier("1");
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.setNationalProviderIdentifier("2");
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different nationalProviderIdentifier !");
    }

    /**
     * Test equals with different providerId
     */
    @Test
    void equalsDifferentProviderId() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.addProviderId("1");
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.addProviderId("2");
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different providerId !");
    }

    /**
     * Test equals with different purposeOfUse
     */
    @Test
    void equalsDifferentPurposeOfUse() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setPurposeOfUse(new CodedValue("1", "1"));
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.setPurposeOfUse(new CodedValue("2", "2"));
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different purposeOfUse !");
    }

    /**
     * Test equals with different subjectRole
     */
    @Test
    void equalsDifferentSubjectRole() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setSubjectRole(new CodedValue("1", "1"));
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.setSubjectRole(new CodedValue("2", "2"));
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different subjectRole !");
    }

    /**
     * Test equals with different resourceId
     */
    @Test
    void equalsDifferentResourceId() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setResourceId("1");
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.setResourceId("2");
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different resourceId !");
    }

    /**
     * Test equals with different onBehalfOf
     */
    @Test
    void equalsDifferentOnBehalfOf() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        accessTokenExtension.setOnBehalfOf("1");
        AccessTokenExtension accessTokenExtension2 = new AccessTokenExtension();
        accessTokenExtension.setOnBehalfOf("2");
        assertFalse(accessTokenExtension.equals(accessTokenExtension2), "Objects shall not be equal if they have different onBehalfOf !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        AccessTokenExtension accessTokenExtension = new AccessTokenExtension();
        assertNotNull(accessTokenExtension.hashCode(), "Generated hashCode shall not be null !");
    }


}
