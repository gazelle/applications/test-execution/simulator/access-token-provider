package net.ihe.gazelle.app.accesstokenproviderapi.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SignatureTest {

    private static final String ALGORITHM = "RSAPOUETPOUET";

    /**
     * Test algorithm getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-008")
    void getAlgorithm(){
        Signature signature = new Signature(ALGORITHM) {};
        assertEquals(ALGORITHM, signature.getAlgorithm(), "Getter shall return the value of algorithm !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        Signature signature = new Signature(ALGORITHM) {};
        assertNotNull(signature.hashCode(), "Generated hashCode shall not be null !");
    }

}
