package net.ihe.gazelle.app.accesstokenproviderapi.application;

import net.ihe.gazelle.app.audienceretriever.application.AudienceSecretRetriever;

import java.util.HashMap;
import java.util.Map;

/**
 * Class to test the retriever of the audience secret
 */
public class AudienceSecretRetrieverTestImpl implements AudienceSecretRetriever {

    private Map<String, String> registry = new HashMap<>();

    /**
     * Constructor
     */
    public AudienceSecretRetrieverTestImpl() {
    }

    /**
     * Add a new audience
     *
     * @param audience String element
     * @param secret   String element
     */
    public void addAudience(String audience, String secret){
        registry.put(audience, secret);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String retrieveSecretForAudience(String audience) {
        return registry.get(audience);
    }
}
