package net.ihe.gazelle.app.accesstokenproviderapi.business;

import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.sb.iua.business.TokenType;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AccessTokenRequestTest {

    private static final String ISSUER = "issuer";
    private static final String SUBJECT = "subject";
    private static final String AUDIENCE = "audience";
    private static final Duration DURATION = Duration.ofMinutes(1);
    private static final TokenType TOKEN_TYPE = TokenType.JWT;
    private static final SymmetricSignature SIGNATURE = new SymmetricSignature("RSALALA", "SECRET");
    private static final AccessTokenExtension EXTENSION = new AccessTokenExtension();


    /**
     * Test issuer getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-001")
    void getIssuer() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertEquals(ISSUER, accessTokenRequest.getIssuer(), "Getter shall return the value of issuer !");
    }

    /**
     * Test subject getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-002")
    void getSubject() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertEquals(SUBJECT, accessTokenRequest.getSubject(), "Getter shall return the value of subject !");
    }

    /**
     * Test audience getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-003")
    void getAudience() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertEquals(AUDIENCE, accessTokenRequest.getAudience(), "Getter shall return the value of audience !");
    }

    /**
     * Test validityTime getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-004")
    void getValidityTime() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertEquals(DURATION, accessTokenRequest.getValidityTime(), "Getter shall return the value of validityTime !");
    }

    /**
     * Test tokenType getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-007")
    void getTokenType() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertEquals(TOKEN_TYPE, accessTokenRequest.getTokenType(), "Getter shall return the value of tokenType !");
    }

    /**
     * Test signature getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-005")
    void signature() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setSignature(SIGNATURE);
        assertEquals(SIGNATURE, accessTokenRequest.getSignature(), "Setter shall change the value of signature !");
    }

    /**
     * Test extension getter and setter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-005")
    void extension() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest.setExtension(EXTENSION);
        assertEquals(EXTENSION, accessTokenRequest.getExtension(), "Setter shall change the value of extension !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertFalse(accessTokenRequest.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertTrue(accessTokenRequest.equals(accessTokenRequest), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertFalse(accessTokenRequest.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different issuer
     */
    @Test
    void equalsDifferentIssuer() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        AccessTokenRequest accessTokenRequest2 = new AccessTokenRequest("ISSUER2", SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertFalse(accessTokenRequest.equals(accessTokenRequest2), "Objects shall not be equal if they have different issuer !");
    }

    /**
     * Test equals with different subject
     */
    @Test
    void equalsDifferentSubject() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        AccessTokenRequest accessTokenRequest2 = new AccessTokenRequest(ISSUER, "SUBJECT2", AUDIENCE, DURATION, TOKEN_TYPE);
        assertFalse(accessTokenRequest.equals(accessTokenRequest2), "Objects shall not be equal if they have different subject !");
    }

    /**
     * Test equals with different audience
     */
    @Test
    void equalsDifferentAudience() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        AccessTokenRequest accessTokenRequest2 = new AccessTokenRequest(ISSUER, SUBJECT, "AUDIENCE2", DURATION, TOKEN_TYPE);
        assertFalse(accessTokenRequest.equals(accessTokenRequest2), "Objects shall not be equal if they have different audience !");
    }

    /**
     * Test equals with different validityTime
     */
    @Test
    void equalsDifferentValidityTime() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        AccessTokenRequest accessTokenRequest2 = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, Duration.ofDays(3), TOKEN_TYPE);
        assertFalse(accessTokenRequest.equals(accessTokenRequest2), "Objects shall not be equal if they have different validityTime !");
    }

    /**
     * Test equals with different tokenType
     */
    @Test
    void equalsDifferentTokenType() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        AccessTokenRequest accessTokenRequest2 = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TokenType.SAML);
        assertFalse(accessTokenRequest.equals(accessTokenRequest2), "Objects shall not be equal if they have different tokenType !");
    }

    /**
     * Test equals with different signature
     */
    @Test
    void equalsDifferentSignature() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        AccessTokenRequest accessTokenRequest2 = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest2.setSignature(new SymmetricSignature("RSA", "secret"));
        assertFalse(accessTokenRequest.equals(accessTokenRequest2), "Objects shall not be equal if they have different signature !");
    }

    /**
     * Test equals with different extension
     */
    @Test
    void equalsDifferentExtension() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        AccessTokenRequest accessTokenRequest2 = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        accessTokenRequest2.setExtension(new AccessTokenExtension());
        assertFalse(accessTokenRequest.equals(accessTokenRequest2), "Objects shall not be equal if they have different extension !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest(ISSUER, SUBJECT, AUDIENCE, DURATION, TOKEN_TYPE);
        assertNotNull(accessTokenRequest.hashCode(), "Generated hashCode shall not be null !");
    }


}
