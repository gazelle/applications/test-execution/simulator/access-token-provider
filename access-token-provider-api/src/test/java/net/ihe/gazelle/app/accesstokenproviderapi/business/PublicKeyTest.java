package net.ihe.gazelle.app.accesstokenproviderapi.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PublicKeyTest {

    private static final byte[] KEY = "key".getBytes();

    /**
     * Test key getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-031")
    void getKey() {
        PublicKey publicKey = new PublicKey(KEY);
        assertArrayEquals(KEY, publicKey.getKey(), "Getter shall return the value of key !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        PublicKey publicKey = new PublicKey(KEY);
        assertFalse(publicKey.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        PublicKey publicKey = new PublicKey(KEY);
        assertTrue(publicKey.equals(publicKey), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        PublicKey publicKey = new PublicKey(KEY);
        assertFalse(publicKey.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different issuer
     */
    @Test
    void equalsDifferentIssuer() {
        PublicKey publicKey = new PublicKey(KEY);
        PublicKey publicKey2 = new PublicKey("KEY".getBytes());
        assertFalse(publicKey.equals(publicKey2), "Objects shall not be equal if they have different issuer !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        PublicKey publicKey = new PublicKey(KEY);
        assertNotNull(publicKey.hashCode(), "Generated hashCode shall not be null !");
    }

}
