package net.ihe.gazelle.app.accesstokenproviderapi.business;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CodedValueTest {

    private static final String CODE = "100000007";
    private static final String CODE_SYSTEM = "2.16.840.1.113883.3.3478.6.1";
    private static final String CODE_SYSTEM_NAME = "ACC-Internal";
    private static final String DISPLAY_NAME = "Hematoma Size";

    /**
     * Test code getter.
     */
    @Test
    void getCode(){
        CodedValue codedValue = new CodedValue(CODE, CODE_SYSTEM);
        assertEquals(CODE, codedValue.getCode(), "Getter shall return the value of code !");
    }

    /**
     * Test codeSystem getter.
     */
    @Test
    void getCodeSystem(){
        CodedValue codedValue = new CodedValue(CODE, CODE_SYSTEM);
        assertEquals(CODE_SYSTEM, codedValue.getCodeSystem(), "Getter shall return the value of codeSystem !");
    }

    /**
     * Test codeSystemName getter and setter.
     */
    @Test
    void codeSystemName(){
        CodedValue codedValue = new CodedValue(CODE, CODE_SYSTEM);
        codedValue.setCodeSystemName(CODE_SYSTEM_NAME);
        assertEquals(CODE_SYSTEM_NAME, codedValue.getCodeSystemName());
    }

    /**
     * Test displayName getter and setter.
     */
    @Test
    void codeDisplayName(){
        CodedValue codedValue = new CodedValue(CODE, CODE_SYSTEM);
        codedValue.setDisplayName(DISPLAY_NAME);
        assertEquals(DISPLAY_NAME, codedValue.getDisplayName());
    }

}
