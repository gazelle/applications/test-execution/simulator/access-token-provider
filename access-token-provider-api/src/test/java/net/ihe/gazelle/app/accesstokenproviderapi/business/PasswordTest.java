package net.ihe.gazelle.app.accesstokenproviderapi.business;

import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PasswordTest {

    private static final byte[] VALUE = "value".getBytes();

    /**
     * Test value getter.
     */
    @Test
    @Covers(requirements = "TOKENPROV-031")
    void getKey() {
        Password publicKey = new Password(VALUE);
        assertArrayEquals(VALUE, publicKey.getValue(), "Getter shall return the value of value !");
    }

    /**
     * Test equals with null.
     */
    @Test
    void equalsNull() {
        Password publicKey = new Password(VALUE);
        assertFalse(publicKey.equals(null), "Object shall not be equal to null !");
    }

    /**
     * Test equals with itself.
     */
    @Test
    void equalsItself() {
        Password publicKey = new Password(VALUE);
        assertTrue(publicKey.equals(publicKey), "Object shall be equal to itself !");
    }

    /**
     * Test equals with another class instance.
     */
    @Test
    void equalsOtherClass() {
        Password publicKey = new Password(VALUE);
        assertFalse(publicKey.equals(34), "Object shall not be equal to an instance of another class !");
    }

    /**
     * Test equals with different issuer
     */
    @Test
    void equalsDifferentIssuer() {
        Password publicKey = new Password(VALUE);
        Password publicKey2 = new Password("VALUE".getBytes());
        assertFalse(publicKey.equals(publicKey2), "Objects shall not be equal if they have different issuer !");
    }

    /**
     * Test for the hashcode method
     */
    @Test
    void testHashCode() {
        Password publicKey = new Password(VALUE);
        assertNotNull(publicKey.hashCode(), "Generated hashCode shall not be null !");
    }

}
